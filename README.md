# MusicSearch #

MusicSearch - это сайт, позволяющий искать информацию о музыкальных композициях, альбомах и артистах.

Главной особенностью сайта, является функция AutoFill, предназначенная для администраторов сайта, которая автоматически ищет информацию об указанном администратором исполнителе в интернете и заполняет ею базу данных. Таким образом, если администрация сайта захочет добавить информацию на сайт, к примеру, о группе Gorillaz, всё что нужно сделать это перейти на вкладку "Create new Author", ввести название группы в поле "Name" и нажать кнопку "Create". Далее функция AutoFill автоматически соберёт информацию о данной группе, занесёт в базу данных все названия и обложки альбомов группы, все названия и тексты песен этой группы, также, автоматически будут созданы web-страницы для отображения этой информации. Вся заполненная информация может быть редактирована вручную.

Демонстрация работы AutoFill:

![authors1.png](https://bitbucket.org/repo/RBearg/images/220750907-authors1.png)
*Чтобы добавить нового исполнителя/группу переходим по ссылке "Create new author"*

![authors2.png](https://bitbucket.org/repo/RBearg/images/612173344-authors2.png)
*Вводим имя исполнителя/группы и жмём кнопку "Create"*

![authors3.png](https://bitbucket.org/repo/RBearg/images/3040610029-authors3.png)
*Система запускает асинхронный процесс, который выполняет функцию парсинга данных с разных сайтов, заполняет базу данных полученной информацией и создаёт web-страницы для этой информации.*

![authors4.png](https://bitbucket.org/repo/RBearg/images/3996031033-authors4.png)

*В среднем, вся информация об артисте загружается за 50 секунд. Так как процесс происходит в фоновом режиме, в это время вы можете без проблем пользоваться сайтом.*

**Для данного проекта были использованы следующие технологии:**

* C#
* ASP.NET
* EntityFramework
* SQL Database
* HTML
* HTML agility pack
* CSS
* Bootstrap