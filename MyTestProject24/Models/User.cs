﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTestProject24.Models
{
    public class User
    {
        public int UserID { get; set; }

        public string Name { get; set; }

        public string Hometown { get; set; }

        public string Information { get; set; }

        [Required]
        public string Avatar { get; set; }

        virtual public List<Post> Posts { get; set; } 

        virtual public List<Track> FavoriteSongs { get; set; }
    }
}
