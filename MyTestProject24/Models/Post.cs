﻿

using System;

namespace MyTestProject24.Models
{
    public class Post
    {
        public int PostID { get; set; }

        public int UserID { get; set; }

        public string Text { get; set; }

        public DateTime DataTime { get; set; }   

        virtual public User User { get; set; }
    }
}