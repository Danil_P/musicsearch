﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyTestProject24.Models
{
    public class Album
    {
        public int AlbumID { get; set; }
        
        public int AuthorID { get; set; }

        [Required()]
        public string Name { get; set; }

        [Required()]
        public string Image { get; set; }

        virtual public Author Author { get; set; }

        virtual public List<Track> Tracks { get; set; }
    }
}