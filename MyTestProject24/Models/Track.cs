﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTestProject24.Models
{
    public class Track
    {
        public int TrackID { get; set; }

        public int AlbumID { get; set; }

        [Required()]
        public string Name { get; set; }
        
        public string Lyrics { get; set; }
        
        virtual public Author Author { get; set; }

        virtual public Album Album { get; set; }
    }
}
