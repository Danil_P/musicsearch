﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyTestProject24.Models
{
    public class Author
    {
        public int AuthorID { get; set; }

        [Required()]
        public string Name { get; set; }

        [Required()]
        public string Photo { get; set; }

        public string Information { get; set; }

        virtual public List<Track> Tracks { get; set; }

        virtual public List<Album> Albums { get; set; }
    }
}