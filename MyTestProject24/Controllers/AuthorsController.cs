﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyTestProject24.DAL;
using MyTestProject24.Models;
using System.Threading;

namespace MyTestProject24.Controllers
{
    public class AuthorsController : Controller
    {
        private UserContext db = new UserContext();
        private static int currentId;

        // GET: Authors
        public ActionResult Index()
        {
            return View(db.Authors.ToList());
        }

        // GET: Authors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Author author = db.Authors.Find(id);
            if (author == null)
            {
                return HttpNotFound();
            }
            return View(author);
        }

        // GET: Authors/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Authors/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AuthorID,Name,Photo")] Author author)
        {
            if (ModelState.IsValid)
            {
                db.Authors.Add(author);
                db.SaveChanges();
                currentId = author.AuthorID;
                Thread t = new Thread(FillAlbums);
                t.Start();
                return RedirectToAction("Profile", new { id = author.AuthorID});
            }



            return RedirectToAction("Profile", new { id = author.AuthorID });
        }


        // GET: Authors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Author author = db.Authors.Find(id);
            if (author == null)
            {
                return HttpNotFound();
            }
            return View(author);
        }

        // POST: Authors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Author author = db.Authors.Find(id);
            db.Authors.Remove(author);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Profile(int id)
        {
            var author = db.Authors.Find(id);
            if (author == null) return HttpNotFound();
            if (author.Information == null)
            {
                author.Information = WebSearcher.GetArtistInformation(author.Name, 1);
                db.SaveChanges();
            }
            return View(author);
        }

        static void FillAlbums()
        {
            int id = currentId; 
            UserContext db = new UserContext();
            string authorName = db.Authors.Find(id).Name;
            List<string> authorAlbums = WebSearcher.GetAlbumsOfArtist(authorName);
            foreach(var album in authorAlbums)
            {
                Album currentAlbum = new Album();
                currentAlbum.AuthorID = id;
                currentAlbum.Name = album;
                currentAlbum.Image = WebSearcher.GetImageOfAlbum(authorName, album);

                db.Albums.Add(currentAlbum);
                db.SaveChanges();

                //IT WAS HIDDEN
                List<string> trackList = WebSearcher.GetTrackListOfAlbum(authorName, album);
                
                foreach(var track in trackList)
                {
                    Track currentTrack = new Track();
                    currentTrack.AlbumID = currentAlbum.AlbumID;
                    currentTrack.Author = db.Authors.Find(id);
                    currentTrack.Name = track;
                    currentTrack.Lyrics = WebSearcher.GetLyricsOfSong(authorName, track);
                    db.Tracks.Add(currentTrack);
                    db.SaveChanges();
                }
                //IT WAS HIDDEN
                if (db.Albums.Find(currentAlbum.AlbumID).Tracks == null)
                {
                    db.Albums.Remove(currentAlbum);
                    db.SaveChanges();
                }
            }

        }        


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
