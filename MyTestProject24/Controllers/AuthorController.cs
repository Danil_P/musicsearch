﻿using MyTestProject24.DAL;
using MyTestProject24.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyTestProject24.Controllers
{
    public class AuthorController : Controller
    {
        Repository<Author> db = new Repository<Author>();
        // GET: Author

        public ActionResult Index()
        {
            return View(db.GetAll());
        }

        // GET: Author/Details/5
        public ActionResult Profile(int id)
        {
            var author = db.Get(id);
            if(author == null)
            {
                return RedirectToAction("Index","Home");
            }
            return View(author);
        }

        // GET: Author/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Author/Create
        [HttpPost]
        public ActionResult Create(Author author)
        {
            try
            {
                db.Add(author);
                db.SaveChanges();
            }
            catch (Exception){            }

            return RedirectToAction("Index");
        }


        // GET: Author/Edit/5
        public ActionResult Edit(int id)
        {
            var author = db.Get(id);
            if(author == null)
            {
                return HttpNotFound();
            } 
            return View(author);
        }

        // POST: Author/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Author author)
        {
            try
            {
                var changeAuthor = db.Get(id);
                changeAuthor = author;
            }
            catch
            {
            }
            return RedirectToAction("Index");
        }

        // GET: Author/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Author/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
