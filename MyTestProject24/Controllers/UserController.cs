﻿using MyTestProject24.DAL;
using MyTestProject24.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyTestProject24.Controllers
{
    public class UserController : Controller
    {
        UserRepository db = new UserRepository();
        //Repository<Post> dbPost = new Repository<Post>();
        // GET: UserList
        [Route("users")]
        public ActionResult Index()
        {
            foreach (var item in db.GetAll())
            {
                if (item == null || item.Avatar == null || item.Name == null)
                {
                    db.Remove(item.UserID);
                    db.SaveChanges();
                }
            }
            return View(db.GetAll());
        }

        //GET: Create
        public ActionResult Create()
        {
            return View();
        }

        //POST: Create
        [HttpPost]
        public ActionResult Create(User user)
        {
            db.Add(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //GET : User/Delete/id
        [Route("user/delete/{id:int}")]
        public ActionResult Delete(int id)
        {
            if (db.Get(id) == null)
                return HttpNotFound();
            db.Remove(id);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [Route("prof/{id:int}")]
        public ActionResult Prof(int id)
        {
            var user = db.Get(id);
            if(user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        [Route("Profile/{id:int}")]
        public ActionResult Profile(int id)
        {
            var user = db.Get(id);
            if(user == null)
            {
                return HttpNotFound();
            }

            return View(user);
        }

        public ActionResult Search(string searchString)
        {
            if (searchString == null)
            {
                return HttpNotFound();
            }

            var userList = db.GetAll().Where(m => m.Name.ToUpper().Contains(searchString.ToUpper()));
            ViewBag.authorList = db.context.Authors.Where(m => m.Name.ToUpper().Contains(searchString.ToUpper())).ToList();
            return View(userList);
        }


    }
}