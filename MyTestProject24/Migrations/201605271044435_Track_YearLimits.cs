namespace MyTestProject24.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Track_YearLimits : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Tracks", "Album_AlbumID", "dbo.Albums");
            DropIndex("dbo.Tracks", new[] { "Album_AlbumID" });
            RenameColumn(table: "dbo.Tracks", name: "Album_AlbumID", newName: "AlbumID");
            AlterColumn("dbo.Tracks", "AlbumID", c => c.Int(nullable: false));
            CreateIndex("dbo.Tracks", "AlbumID");
            AddForeignKey("dbo.Tracks", "AlbumID", "dbo.Albums", "AlbumID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tracks", "AlbumID", "dbo.Albums");
            DropIndex("dbo.Tracks", new[] { "AlbumID" });
            AlterColumn("dbo.Tracks", "AlbumID", c => c.Int());
            RenameColumn(table: "dbo.Tracks", name: "AlbumID", newName: "Album_AlbumID");
            CreateIndex("dbo.Tracks", "Album_AlbumID");
            AddForeignKey("dbo.Tracks", "Album_AlbumID", "dbo.Albums", "AlbumID");
        }
    }
}
