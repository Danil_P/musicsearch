namespace MyTestProject24.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Album_AuthorID : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Albums", "Author_AuthorID", "dbo.Authors");
            DropIndex("dbo.Albums", new[] { "Author_AuthorID" });
            RenameColumn(table: "dbo.Albums", name: "Author_AuthorID", newName: "AuthorID");
            AlterColumn("dbo.Albums", "AuthorID", c => c.Int(nullable: false));
            CreateIndex("dbo.Albums", "AuthorID");
            AddForeignKey("dbo.Albums", "AuthorID", "dbo.Authors", "AuthorID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Albums", "AuthorID", "dbo.Authors");
            DropIndex("dbo.Albums", new[] { "AuthorID" });
            AlterColumn("dbo.Albums", "AuthorID", c => c.Int());
            RenameColumn(table: "dbo.Albums", name: "AuthorID", newName: "Author_AuthorID");
            CreateIndex("dbo.Albums", "Author_AuthorID");
            AddForeignKey("dbo.Albums", "Author_AuthorID", "dbo.Authors", "AuthorID");
        }
    }
}
