namespace MyTestProject24.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Track_Lyrics : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tracks", "Lyrics", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tracks", "Lyrics");
        }
    }
}
