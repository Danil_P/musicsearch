namespace MyTestProject24.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTracksAlbumsAuthors : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Albums",
                c => new
                    {
                        AlbumID = c.Int(nullable: false, identity: true),
                        Photo = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        Image = c.String(nullable: false),
                        Author_AuthorID = c.Int(),
                    })
                .PrimaryKey(t => t.AlbumID)
                .ForeignKey("dbo.Authors", t => t.Author_AuthorID)
                .Index(t => t.Author_AuthorID);
            
            CreateTable(
                "dbo.Authors",
                c => new
                    {
                        AuthorID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.AuthorID);
            
            CreateTable(
                "dbo.Tracks",
                c => new
                    {
                        TrackID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Album_AlbumID = c.Int(),
                        Author_AuthorID = c.Int(),
                        User_UserID = c.Int(),
                    })
                .PrimaryKey(t => t.TrackID)
                .ForeignKey("dbo.Albums", t => t.Album_AlbumID)
                .ForeignKey("dbo.Authors", t => t.Author_AuthorID)
                .ForeignKey("dbo.Users", t => t.User_UserID)
                .Index(t => t.Album_AlbumID)
                .Index(t => t.Author_AuthorID)
                .Index(t => t.User_UserID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tracks", "User_UserID", "dbo.Users");
            DropForeignKey("dbo.Tracks", "Author_AuthorID", "dbo.Authors");
            DropForeignKey("dbo.Tracks", "Album_AlbumID", "dbo.Albums");
            DropForeignKey("dbo.Albums", "Author_AuthorID", "dbo.Authors");
            DropIndex("dbo.Tracks", new[] { "User_UserID" });
            DropIndex("dbo.Tracks", new[] { "Author_AuthorID" });
            DropIndex("dbo.Tracks", new[] { "Album_AlbumID" });
            DropIndex("dbo.Albums", new[] { "Author_AuthorID" });
            DropTable("dbo.Tracks");
            DropTable("dbo.Authors");
            DropTable("dbo.Albums");
        }
    }
}
