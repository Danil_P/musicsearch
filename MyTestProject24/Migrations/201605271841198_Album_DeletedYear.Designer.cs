// <auto-generated />
namespace MyTestProject24.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Album_DeletedYear : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Album_DeletedYear));
        
        string IMigrationMetadata.Id
        {
            get { return "201605271841198_Album_DeletedYear"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
