namespace MyTestProject24.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTracksAlbumsAuthors1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Authors", "Photo", c => c.String(nullable: false));
            DropColumn("dbo.Albums", "Photo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Albums", "Photo", c => c.String(nullable: false));
            DropColumn("dbo.Authors", "Photo");
        }
    }
}
