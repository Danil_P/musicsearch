namespace MyTestProject24.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Author_Information1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Authors", "Information", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Authors", "Information");
        }
    }
}
