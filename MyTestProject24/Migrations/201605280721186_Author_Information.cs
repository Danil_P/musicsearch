namespace MyTestProject24.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Author_Information : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Information", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "Information");
        }
    }
}
