// <auto-generated />
namespace MyTestProject24.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddedTracksAlbumsAuthors : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddedTracksAlbumsAuthors));
        
        string IMigrationMetadata.Id
        {
            get { return "201605251427462_AddedTracksAlbumsAuthors"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
