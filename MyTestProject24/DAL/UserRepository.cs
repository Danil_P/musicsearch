﻿using MyTestProject24.Models;
using System.Collections.Generic;
using System.Linq;

namespace MyTestProject24.DAL
{
    public class UserRepository : Repository<User>
    {
       /* public override void Add(User entity)
        {
            context.Users.Add(entity);
            base.Add(entity);
        }*/

        public List<User> GetByName(string name)
        {
            return DbSet.Where(a => a.Name.Contains(name)).ToList();
        }
    }
}
