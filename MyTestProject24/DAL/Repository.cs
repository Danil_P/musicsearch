﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTestProject24.DAL
{
    public class Repository<T> where T : class
    {
        private bool disposed = false;
        public UserContext context = null;
        protected DbSet<T> DbSet { get; set; }

        public Repository()
        {
            context = new UserContext();
            DbSet = context.Set<T>();
        }

        public Repository(UserContext context)
        {
            this.context = context;
            DbSet = context.Set<T>();
        }

        public List<T> GetAll()
        {
            return DbSet.ToList();
        }

        public T Get(int id)
        {
            return DbSet.Find(id);
        }

        virtual public void Add(T entity)
        {
            DbSet.Add(entity);
            return;
        }

        virtual public void Remove(int id)
        {
            DbSet.Remove(DbSet.Find(id));
            return;
        }

        public void Update(T entity)
        {
            context.Entry<T>(entity).State = EntityState.Modified;
        }

        public void SaveChanges()
        {
            context.SaveChanges();
            return;
        }

        public void Dispose()
        {
            if (!disposed)
            {
                context.Dispose();
                disposed = true;
            }
        }
    }
}
