﻿using System.Text;
using HtmlAgilityPack;


public static class WikipediaWorker
{

    public static string GetArtistInformation(string artistName, int numberOfParagraphs) //
    {
        artistName = artistName.Replace(' ', '_');
        
        string htmlRef = "https://en.wikipedia.org/wiki/" + artistName;
        HtmlDocument htmlDocument = new HtmlDocument();
        var web = new HtmlWeb
        {
            AutoDetectEncoding = false,
            OverrideEncoding = Encoding.UTF8,
        };
        htmlDocument = web.Load(htmlRef);

        HtmlNodeCollection NoAltElements = htmlDocument.DocumentNode.SelectNodes("//div[@class='mw-content-ltr']/p");

        int numberOfCurrentParagraph = 0;
        string outputText = "";
        // проверка на наличие найденных узлов
        if (NoAltElements != null)
        {
            foreach (HtmlNode HN in NoAltElements)
            {
                numberOfCurrentParagraph++;

                var withoutBrackets = HN.InnerText;
                for(int i = 0; i<withoutBrackets.Length; i++)
                {
                    if (withoutBrackets[i] == '[')
                        while (i<withoutBrackets.Length && withoutBrackets[i] != ']') i++;
                    else outputText += withoutBrackets[i];

                }

                outputText += "\n";
                //outputText += HN.InnerText + "\n";

                if (numberOfCurrentParagraph >= numberOfParagraphs) break;
            }
        }

        return outputText;
}


}