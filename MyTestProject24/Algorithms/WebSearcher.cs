﻿using System.Text;
using HtmlAgilityPack;
using System.Collections.Generic;

public static class WebSearcher
{

    public static string GetArtistInformation(string artistName, int numberOfParagraphs) //
    {
        artistName = artistName.Replace(' ', '_');

        string htmlRef = "https://en.wikipedia.org/wiki/" + artistName;
        HtmlDocument htmlDocument = new HtmlDocument();
        var web = new HtmlWeb
        {
            AutoDetectEncoding = false,
            OverrideEncoding = Encoding.UTF8,
        };
        try
        {
            htmlDocument = web.Load(htmlRef);
        }
        catch
        {
            return "";
        }
        HtmlNodeCollection NoAltElements = htmlDocument.DocumentNode.SelectNodes("//div[@class='mw-content-ltr']/p");

        int numberOfCurrentParagraph = 0;
        string outputText = "";
        // проверка на наличие найденных узлов
        if (NoAltElements != null)
        {
            foreach (HtmlNode HN in NoAltElements)
            {
                numberOfCurrentParagraph++;

                var withoutBrackets = HN.InnerText;
                for (int i = 0; i < withoutBrackets.Length; i++)
                {
                    if (withoutBrackets[i] == '[')
                        while (i < withoutBrackets.Length && withoutBrackets[i] != ']') i++;
                    else outputText += withoutBrackets[i];

                }

                outputText += "\n";
                //outputText += HN.InnerText + "\n";

                if (numberOfCurrentParagraph >= numberOfParagraphs) break;
            }
        }
        if (outputText.Contains("may refer to:")) return "";
        return outputText;
    }

    public static string GetLyricsOfSong(string artistName, string songTitle)
    {

        string[] songNameWords = songTitle.Split(' ');

        songTitle = "";
        for (int i = 0; i < songNameWords.Length; i++)
        {
            if (songNameWords[i] == null && songNameWords[i][0] == '(')
                break;
            else
                if (i != 0)
                songTitle = songTitle + "_" + songNameWords[i];
            else
                songTitle = songNameWords[0];

        }

        songTitle = songTitle.Replace(' ', '_');
        artistName = artistName.Replace(' ', '_');

        string htmlRef = "http://www.lyricsmode.com/lyrics/" + artistName[0] + "/" + artistName + "/" + songTitle + ".html";
        HtmlDocument htmlDocument = new HtmlDocument();
        var web = new HtmlWeb
        {
            AutoDetectEncoding = false,
            OverrideEncoding = Encoding.UTF8,
        };
        try
        {
            htmlDocument = web.Load(htmlRef);
        }
        catch
        {
            return "";
        }
        HtmlNodeCollection NoAltElements = htmlDocument.DocumentNode.SelectNodes("//p[@id='lyrics_text']");
        string outputText = "";
        // проверка на наличие найденных узлов
        if (NoAltElements != null)
        {
            foreach (HtmlNode HN in NoAltElements)
            {
                outputText += HN.InnerText;
            }
        }
        return outputText;
    }

    public static List<string> GetTrackListOfAlbum(string artistName, string albumTitle)
    {
        List<string> trackList = new List<string>();
        albumTitle = albumTitle.Replace(' ', '+');
        artistName = artistName.Replace(' ', '+');

        string htmlRef = "http://www.last.fm/music/" + artistName + "/" + albumTitle;
        HtmlDocument htmlDocument = new HtmlDocument();
        var web = new HtmlWeb
        {
            AutoDetectEncoding = false,
            OverrideEncoding = Encoding.UTF8,
        };

        try
        {
            htmlDocument = web.Load(htmlRef);
        }
        catch
        {
            return new List<string>();
        }
        HtmlNodeCollection NoAltElements = htmlDocument.DocumentNode.SelectNodes("//a[@class='link-block-target']");
        // проверка на наличие найденных узлов
        if (NoAltElements != null)
        {
            foreach (HtmlNode HN in NoAltElements)
            {
                trackList.Add(HN.InnerText);
            }
        }
        return trackList;
    }

    public static List<string> GetAlbumsOfArtist(string artistName)
    {
        List<string> albumList = new List<string>();
        artistName = artistName.Replace(' ', '+');

        string htmlRef = "http://www.last.fm/ru/music/" + artistName + "/+albums";
        HtmlDocument htmlDocument = new HtmlDocument();
        var web = new HtmlWeb
        {
            AutoDetectEncoding = false,
            OverrideEncoding = Encoding.UTF8,
        };
        try
        {
            htmlDocument = web.Load(htmlRef);
        }
        catch
        {
            return new List<string>();
        }

        HtmlNodeCollection NoAltElements = htmlDocument.DocumentNode.SelectNodes("//p[@class='album-grid-item-main-text']");
        // проверка на наличие найденных узлов
        if (NoAltElements != null)
        {
            foreach (HtmlNode HN in NoAltElements)
            {
                albumList.Add(HN.InnerText);
            }
        }
        return albumList;
    }

    public static string GetImageOfAlbum(string artistName, string albumTitle)
    {
        albumTitle = albumTitle.Replace(' ', '+');
        artistName = artistName.Replace(' ', '+');

        string htmlRef = "http://www.last.fm/music/" + artistName + "/" + albumTitle;
        HtmlDocument htmlDocument = new HtmlDocument();
        var web = new HtmlWeb
        {
            AutoDetectEncoding = false,
            OverrideEncoding = Encoding.UTF8,
        };

        try
        {
            htmlDocument = web.Load(htmlRef);
        }
        catch
        {
            return "https://cdn2.iconfinder.com/data/icons/flat-icons-19/512/Vinyl.png";
        }

        string htmlOutput = "";
        HtmlNodeCollection NoAltElements = htmlDocument.DocumentNode.SelectNodes("//*[@id='content']/header/div[2]/div/div[1]/div/div/a/img");

        if (NoAltElements != null)
        {
            foreach (HtmlNode HN in NoAltElements)
            {
                htmlOutput += HN.OuterHtml;
            }
        }


        if (htmlOutput.Length <= 10)
            return "https://cdn2.iconfinder.com/data/icons/flat-icons-19/512/Vinyl.png";



        int i = 10;

        string referenceToImage = "";
        while (i < htmlOutput.Length && htmlOutput[i] != '"')
        {
            referenceToImage += htmlOutput[i];
            i++;
        }



        return referenceToImage;
    }


}